## Installation of requirements
Create a virtualenv. venv can be used if you don't have other preference:
```bash
sudo apt install python3-venv
python3 -m venv npi-project-env
```

Source environment, update pip, and install dependencies:
```bash
source npi-project-env/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

## How to run N-Queens Problems Experiments:
#### Training:
Following will train the NPI, then report training error and plot learning curve.
```bash
python main.py --task=nqueens --do_train=True --do_eval=False
```

#### Testing:

Following will test the NPI, then report testing errors.
```bash
python main.py --task=nqueens --do_train=False --do_eval=True 
```

## How to run Dijsktra's Algorithm Experiments:
#### Training:
Following will train the NPI, then report training error and plot learning curve.
```bash
python main.py --task=dijkstra --do_train=True --do_eval=False
```

#### Testing:

Following will test the NPI, then report testing errors.
```bash
python main.py --task=dijkstra --do_train=False --do_eval=True 
```

## How to run TSP Experiments:
To execute this code (not working).
```bash
cd tasks/tsp
python tsp.py
```
The above code is not working. It needs correct execution traces and TSP core to run. The algorithm of TSP is based on Nearest Neighbour approach.
The generate_data.py file generates the graph of node size ‘n’ using random values. These values are dumped into pik data files which will be read by the npi core. It has the exact order of subroutines to be called.
The config.py file has the configuration information, vector/program embedding information, argument depth, argument num and default arg value defined as below:
```python
"ARGUMENT_NUM": 1,            # Maximum Number of ProgramArguments
"ARGUMENT_DEPTH": 2,          # Size of Argument Vector =>One-Hot, Options 0-1, Default (2)
"DEFAULT_ARG_VALUE": 0,       # Default Argument Value 
"PROGRAM_NUM": 6,             # Maximum Number of Subroutines
"PROGRAM_KEY_SIZE": 5,        # Size of the Program Keys
"PROGRAM_EMBEDDING_SIZE": 10  # Size of the Program Embeddings
```
eval.py file Loads in an tsp NPI, and starts a REPL for interactive tsp.  
The trace.py file has core class definition for a trace object => given a pair of integers to add, builds the execution trace, calling the specified subprograms.  
Train.py file has core training script for the TSP task-specific NPI. Instantiates a model, then trains using the precomputed data.  
Tsp.py runs the travelling salesman code.   
The execution traces should be of type [current_node,current_distance] and should be updated at every iteration. The values start from [0,INF] assuming 0 to be the start node. 

## How to run Merge Sort Experiments:
There is no working code for Merge Sort experiments.