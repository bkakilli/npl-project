"""Dijkstra's Shortest Path Algorithm implementation for Nerual Programmer-Interpreter

Author: Burak Kakillioglu
Date:   4/20/19

ToDo:
    - Define graph #done
    - Define states #done
    - Implement solver, which will implement the algorithm and generates the flow
    - Implement graph generator
        1) All vertexes can be reachable
        2) Some vertexes are not reachable
"""

import numpy as np

loglevel = 1

func_dict = {
    'DIJSKTRA': 0,
    'SET_CURRENT': 1,
    'SET_VISITED': 2,
    'BYPASS': 3,
    'UNSET': 4,
    'MOVE': 5
}


def log(msg, level):
    if level > loglevel:
        print (msg)

def index_to_vertex(i):
    # return ['A','B','C','D','E'][i]
    return str(i)

def get_next_unvisited(unvisited, state):
    index_of_unvisited = np.where(unvisited)[0]
    unvisited_vertices_dist = state[unvisited,0]
    which_unvisited = unvisited_vertices_dist.argmin()
    
    v = index_of_unvisited[which_unvisited]
    shortest = state[v,0]
    return v, shortest

def get_unvisited_neighbors(graph, v, unvisited):
    neighbors = np.logical_and(graph[v] > 0, unvisited)
    return np.where(neighbors)[0]

def make_state_space(graph, state, unvisited):
    return np.hstack((graph, unvisited.reshape((len(graph),1)).astype(int), state))

def generate_graph(nodes_count):
    def get_random_cost():
        unique_costs = [1,2,3,4,5,6,7,8]
        not_connected_probability = 0.5

        p = np.ones((len(unique_costs)+1,)) * (1-not_connected_probability) / len(unique_costs)
        p[0] = not_connected_probability

        return np.random.choice([-1] + unique_costs, p=p)

    graph = np.zeros((nodes_count, nodes_count), dtype=int)
    for i in range(nodes_count):
        for j in range(i+1,nodes_count):
            cost = get_random_cost()
            graph[i,j] = cost
            graph[j,i] = cost

    return graph


class DijkstraEnv:

    def __init__(self, graph):
        self.INF = 999

        self.graph = graph
        self.adjacency = (graph > 0)

        self.nodes_count = len(graph)
        self.unvisited = np.ones((self.nodes_count,), dtype=np.bool)
        self.state = np.ones((self.nodes_count, 2), dtype=int) * self.INF
        self.state[:,1] = -1
        self.state[0,0] = 0
        self.v = -1

        # Pointers
        self.UNVISITED_POINTER_ADDR = 0
        self.UNVISITED_POINTER = np.zeros((len(graph,)))
        self.UNVISITED_POINTER[0] = 1

        self.CURRENT_NODE_POINTER_ADDR = 1
        self.CURRENT_NODE_POINTER = np.zeros(len(graph,))
        self.CURRENT_NODE_POINTER[0] = 1
        

    def execute(self, f, args):
        if f == 'UNSET':
            pointer_addr = args[0]
            if pointer_addr == self.UNVISITED_POINTER_ADDR:
                index = self.UNVISITED_POINTER.argmax()
                self.unvisited[index] = False
        elif f == 'MOVE':
            pointer_addr = args[0]
            if pointer_addr == self.UNVISITED_POINTER_ADDR:
                self.UNVISITED_POINTER = np.roll(self.UNVISITED_POINTER, 1)
            elif pointer_addr == self.CURRENT_NODE_POINTER_ADDR:
                self.CURRENT_NODE_POINTER = np.roll(self.CURRENT_NODE_POINTER, 1)
        elif f == 'BYPASS':
            v = self.CURRENT_NODE_POINTER.argmax()
            # OP-2
            neighbors = get_unvisited_neighbors(self.graph, self.v, self.unvisited)
            # print (state)

            # OP-3
            shortest = self.state[v,0]
            edges = self.graph[self.v]
            for n in neighbors:
                if shortest + edges[n] < self.state[n,0]:
                    # Update the state
                    self.state[n,0] = shortest + edges[n]
                    self.state[n,1] = self.v
    
    def make_state_vector(self):
        state_vec = np.concatenate((
            # self.graph.reshape(-1).astype(np.int32),
            # self.adjacency.reshape(-1).astype(np.int32),
            self.state.copy().T.reshape(-1).astype(np.int32),
            self.unvisited.copy().reshape(-1).astype(np.int32),
            self.UNVISITED_POINTER.copy().reshape(-1).astype(np.int32),
            self.CURRENT_NODE_POINTER.copy().reshape(-1).astype(np.int32)
        ), axis=0)
        return state_vec        

    def run():
        graph = self.graph
        unvisited = np.ones((self.nodes_count,), dtype=np.bool)
        state = np.ones((self.nodes_count, 2), dtype=int) * self.INF
        state[:,1] = -1
        state[0,0] = 0
        
        while unvisited.sum() > 0:
            # Visit an unvisited vertex with the smallest distance to the start vertex
            v, shortest = get_next_unvisited(unvisited, state)
            neighbors = get_unvisited_neighbors(graph, v, unvisited)
            log("neighbors: %s" % str([index_to_vertex(i) for i in neighbors] ), level=1)

            edges = graph[v]
            log("Edges: %s" % (str(edges)), level=1)

            for n in neighbors:
                log("Distance to %s is %d" % (index_to_vertex(n), edges[n]), level=1)
                if shortest + edges[n] < state[n,0]:
                    # Update the state
                    state[n,0] = shortest + edges[n]
                    state[n,1] = v
            

            # Mark v as visited
            unvisited[v] = False

        log ("Visited: %s" % (index_to_vertex(v)), level=1)
        log ("Current unvisited: %s"% (unvisited), level=1)
        log ("SS:\n%s" % (str(make_state_space(graph, state, unvisited))), level=1)

# graph = generate_graph(nodes_count)

# # Manual graph for debugging
# # graph = np.array([
# #     [ 0,  6, -1,  1, -1],
# #     [ 6,  0,  5,  2,  2],
# #     [-1,  5,  0, -1,  5],
# #     [ 1,  2, -1,  0,  1],
# #     [-1,  2,  5,  1,  0]
# # ])
# nodes_count = len(graph)
# print (graph)

# unvisited = np.ones((nodes_count,), dtype=np.bool)
# state = np.ones((nodes_count, 2), dtype=int) * INF
# state[:,1] = -1
# state[0,0] = 0