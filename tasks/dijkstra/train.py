"""
train.py

Core training script for the Dijkstra task-specific NPI. Instantiates a model, then trains using
the precomputed data.
"""
from model.npi import NPI
from tasks.dijkstra.dijkstra import DijkstraCore
from tasks.dijkstra.config import CONFIG, get_args
from tasks.dijkstra.d import func_dict, DijkstraEnv
from tqdm import tqdm
import pickle
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

MOVE_PID, UNSET_PID = func_dict['MOVE'], func_dict['UNSET']
DATA_PATH = "tasks/dijkstra/data/train.pik"
LOG_PATH = "tasks/dijkstra/log/"


def train_dijkstra(epochs, verbose=0):
    """
    Instantiates an Dijkstra Core, NPI, then loads and fits model to data.

    :param epochs: Number of epochs to train for.
    """
    # # Load Data
    # with open(DATA_PATH, 'rb') as f:
    #     data = pickle.load(f)

    # # Initialize Dijkstra Core
    # print ('Initializing Dijkstra Core!')
    # core = DijkstraCore()

    # # Initialize NPI Model
    # print ('Initializing NPI Model!')
    # npi = NPI(core, CONFIG, LOG_PATH, verbose=verbose)

    # # Initialize TF Saver
    # saver = tf.train.Saver()

    # loss_list = []
    # term_list = []
    # prog_list = []
    # args_list = []

    # # Initialize TF Session
    # with tf.Session() as sess:
    #     sess.run(tf.global_variables_initializer())

    #     # Start Training
    #     for ep in range(1, epochs + 1):
    #         for i in tqdm(range(len(data)), ncols=100):
    #             # Reset NPI States
    #             npi.reset_state()

    #             # Setup Environment
    #             graph, steps = data[i]
    #             d_pad = DijkstraEnv(graph)
    #             x, y = steps[:-1], steps[1:]

    #             # Run through steps, and fit!
    #             step_def_loss, step_arg_loss, term_acc, prog_acc, = 0.0, 0.0, 0.0, 0.0
    #             arg0_acc, arg1_acc, arg2_acc, num_args = 0.0, 0.0, 0.0, 0
    #             for j in range(len(x)):
    #                 (prog_name_in, prog_in_id), arg, term = x[j]
    #                 (prog_name_out, prog_out_id), arg_out, term_out = y[j]

    #                 # print ("%s(%s) calls %s(%s)" % (prog_name_in, str(arg), prog_name_out, str(arg_out)))

    #                 # Update Environment if MOVE or WRITE
    #                 if prog_in_id == MOVE_PID or prog_in_id == UNSET_PID:
    #                     d_pad.execute(prog_in_id, arg)

    #                 # Get Environment, Argument Vectors
    #                 env_in = [d_pad.make_state_vector()]
    #                 arg_in, arg_out = [get_args(arg, arg_in=True)], get_args(arg_out, arg_in=False)
    #                 prog_in, prog_out = [[prog_in_id]], [prog_out_id]
    #                 term_out = [1] if term_out else [0]

    #                 # Fit!
    #                 if prog_out_id == MOVE_PID or prog_out_id == UNSET_PID:
    #                     loss, t_acc, p_acc, a_acc, _ = sess.run(
    #                         [npi.arg_loss, npi.t_metric, npi.p_metric, npi.a_metrics, npi.arg_train_op],
    #                         feed_dict={npi.env_in: env_in, npi.arg_in: arg_in, npi.prg_in: prog_in,
    #                                 npi.y_prog: prog_out, npi.y_term: term_out,
    #                                 npi.y_args[0]: [arg_out[0]],})
    #                     step_arg_loss += loss
    #                     term_acc += t_acc
    #                     prog_acc += p_acc
    #                     arg0_acc += a_acc[0]
    #                     num_args += 1
    #                 else:
    #                     loss, t_acc, p_acc, _ = sess.run(
    #                         [npi.default_loss, npi.t_metric, npi.p_metric, npi.default_train_op],
    #                         feed_dict={npi.env_in: env_in, npi.arg_in: arg_in, npi.prg_in: prog_in,
    #                                 npi.y_prog: prog_out, npi.y_term: term_out})
    #                     step_def_loss += loss
    #                     term_acc += t_acc
    #                     prog_acc += p_acc

    #             print ("\rEpoch {0:02d} Step {1:03d} Default Step Loss {2:05f}, " \
    #                 "Argument Step Loss {3:05f}, Term: {4:03f}, Prog: {5:03f}, A0: {6:03f}" \
    #                     .format(ep, i, step_def_loss / len(x), step_arg_loss / len(x), term_acc / len(x),
    #                         prog_acc / len(x), arg0_acc / num_args), end='', flush=True)

    #             loss_list.append(step_def_loss / len(x))
    #             term_list.append(term_acc / len(x))
    #             prog_list.append(prog_acc / len(x))
    #             args_list.append(arg0_acc / num_args)


    #         # Save Model
    #         saver.save(sess, 'tasks/dijkstra/log/model.ckpt')

    # stacked = np.vstack((loss_list, term_list, prog_list, args_list)).T
    # np.save('experiment_results.npy', stacked)
    stacked = np.load('experiment_results.npy')

    i = np.arange(len(stacked))+1
    l = stacked[:,0]
    t = 1-stacked[:,1]
    p = 1-stacked[:,2]
    a = 1-stacked[:,3]

    plt.plot(i, l, 'r')
    plt.xlabel('Iteration')
    plt.ylabel('Loss')
    plt.show()

    plt.plot(i, t, 'r', i, p, 'b', i, a, 'g')
    plt.legend(['Termination error', 'Program id error', 'Argument error'])
    plt.xlabel('Iteration')
    plt.ylabel('Error')
    plt.show()
