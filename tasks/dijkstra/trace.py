"""Dijkstra's Algorithm Core of Neural Programmer-Interpreters: https://arxiv.org/abs/1511.06279

Author: Burak Kakillioglu
Date created: 5/2/19

Summary:
This script defines routines for Dijkstra's Algorithm:
    1) v = get_next_unvisited()
    2) n[] = get_unvisited_neighbors_of(v)
    3) update_neighbors(n[])
    4) mark_visited(v)

Implementation details:
    1) Can be implemented as a pointer
    2) Can be implemented as hard-coded for simplicity.
    3) Two WRITE(address, value) subroutine can be called for node update. The calculation of values to update be updated will be hard-coded.
    4) a subroutine WRITE(address, value) can be called by NPI 
"""

"""
next_args = []
next_program, next_args = get_next_unvisited(args=next_args)
next_program, next_args = get_unvisited_neighbors(args=next_args)
next_program, next_args = update_neighbors(args=next_args)
next_program, next_args = mark_visited(args=next_args)


"""
import numpy as np
import tasks.dijkstra.d as dijkstra

class DijkstraTrace:

    def __init__(self, graph):
        self.trace = []
        self.data = []
        self.create_trace(graph)
        self.make_data()

    def append(self, s):

        s['id'] = len(self.trace)
        self.trace.append(s)
    
    def make_data(self):
        for s in self.trace:
            self.data.append(( (s['func'], dijkstra.func_dict[s['func']]), s['return'], s['terminate'] ))

    def __str__(self):

        t_str = ""
        # For each state in trace
        for i in range(len(self.trace)):
            s = self.trace[i]
            t_str += 'Computing function %s: \n' % s['func']
            t_str += 'STATE: %s\n' % (str(s['state_vec'][50:]))
            t_str += 'State In:\n' + str(s['state']) + '\n'
            t_str += 'Unvisited: %s, Current node: %s' % (
                str(s['unvisited'].reshape(-1)), str(s['current_node_pointer'].reshape(-1))
            ) + '\n\n'
            t_str += "ACTION: \n"
            t_str += 'Next program: %s' % ('TERMINATE' if s['terminate'] else self.trace[i+1]['func']) + '\n'
            # t_str += str(s) + '\n'
            t_str += '------------------------\n'
        
        t_str += '\n'
        return t_str

    def __len__(self):
        return len(self.trace)


    def create_trace(self, graph):

        self.graph = graph
        d_pad = dijkstra.DijkstraEnv(graph)

        def new_trace_elem(func, args, terminate=False):
            trace_elem = {
                'func': func,
                'state': d_pad.state.copy(),
                'unvisited': d_pad.unvisited.copy(),
                'unvisited_pointer': d_pad.UNVISITED_POINTER.copy(),
                'current_node_pointer': d_pad.CURRENT_NODE_POINTER.copy(),
                'state_vec': d_pad.make_state_vector(),
                'return': args,
                'terminate': terminate
            }

            return trace_elem

        trace_elem = new_trace_elem('DIJSKTRA', [])
        self.append(trace_elem)

        while d_pad.unvisited.sum() > 0:

            # OP-1
            # Visit an unvisited vertex with the smallest distance to the start vertex
            v, _ = dijkstra.get_next_unvisited(d_pad.unvisited, d_pad.state)
            trace_elem = new_trace_elem('SET_CURRENT', [])
            self.append(trace_elem)
            
            while d_pad.CURRENT_NODE_POINTER.argmax() != v:
                trace_elem = new_trace_elem('MOVE', [d_pad.CURRENT_NODE_POINTER_ADDR])
                self.append(trace_elem)
                d_pad.execute(trace_elem['func'], trace_elem['return'])

            trace_elem = new_trace_elem('BYPASS', [])
            self.append(trace_elem)
            d_pad.execute(trace_elem['func'], trace_elem['return'])
            
            # OP-4
            # Mark v as visited
            trace_elem = new_trace_elem('SET_VISITED', [])
            self.append(trace_elem)

            while d_pad.UNVISITED_POINTER.argmax() != d_pad.CURRENT_NODE_POINTER.argmax():
                trace_elem = new_trace_elem('MOVE', [d_pad.UNVISITED_POINTER_ADDR])
                self.append(trace_elem)
                d_pad.execute(trace_elem['func'], trace_elem['return'])

            trace_elem = new_trace_elem('UNSET', [d_pad.UNVISITED_POINTER_ADDR])
            self.append(trace_elem)
            d_pad.execute(trace_elem['func'], trace_elem['return'])

        # Update last operation
        self.trace[-1]['terminate'] = True

if __name__ == "__main__":
    
    graph = np.array([
        [ 0,  6, -1,  1, -1],
        [ 6,  0,  5,  2,  2],
        [-1,  5,  0, -1,  5],
        [ 1,  2, -1,  0,  1],
        [-1,  2,  5,  1,  0]
    ])

    trace = DijkstraTrace(graph).data
    print (trace)