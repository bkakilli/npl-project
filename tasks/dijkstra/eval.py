"""
eval.py

Loads in an Dijkstra NPI, and starts a REPL for interactive dijkstra.
"""
from model.npi import NPI
from tasks.dijkstra.dijkstra import DijkstraCore
from tasks.dijkstra.config import CONFIG, get_args
from tasks.dijkstra.d import func_dict, DijkstraEnv
import numpy as np
import pickle
import tensorflow as tf

LOG_PATH = "tasks/dijkstra/log/"
CKPT_PATH = "tasks/dijkstra/log/model.ckpt"
TEST_PATH = "tasks/dijkstra/data/test.pik"

MOVE_PID, UNSET_PID = func_dict['MOVE'], func_dict['UNSET']
W_PTRS = {0: "OUT", 1: "CARRY"}
PTRS = {0: "IN1_PTR", 1: "IN2_PTR", 2: "CARRY_PTR", 3: "OUT_PTR"}
R_L = {0: "LEFT", 1: "RIGHT"}


def evaluate_dijkstra():
    """
    Load NPI Model from Checkpoint, and initialize REPL, for interactive carry-dijkstra.
    """
    # Load Data
    with open(TEST_PATH, 'rb') as f:
        data = pickle.load(f)

    # Initialize Dijkstra Core
    core = DijkstraCore()

    # Initialize NPI Model
    npi = NPI(core, CONFIG, LOG_PATH)

    with tf.Session() as sess:
        # Restore from Checkpoint
        saver = tf.train.Saver()
        saver.restore(sess, CKPT_PATH)

        # Run REPL
        repl(sess, npi, data)


def repl(session, npi, data):
    for i in range(data):

        graph, steps = data[i]

        # Reset NPI States
        print ()
        npi.reset_state()

        # Setup Environment
        d_pad = DijkstraEnv(graph)
        prog_name, prog_id, arg, term = 'DIJKSTRA', func_dict['DIJKSTRA'], [], False

        cont = 'c'
        while cont == 'c' or cont == 'C':

            # Update Environment if MOVE or WRITE
            if prog_id == MOVE_PID or prog_id == UNSET_PID:
                d_pad.execute(prog_id, arg)

            # Get Environment, Argument Vectors
            env_in, arg_in, prog_in = [scratch.get_env()], [get_args(arg, arg_in=True)], [[prog_id]]
            t, n_p, n_args = session.run([npi.terminate, npi.program_distribution, npi.arguments],
                                         feed_dict={npi.env_in: env_in, npi.arg_in: arg_in,
                                                    npi.prg_in: prog_in})

            if np.argmax(t) == 1:
                print ('Step: %s, Arguments: %s, Terminate: %s' % (prog_name, a_str, str(True)))
                print ('IN 1: %s, IN 2: %s, CARRY: %s, OUT: %s' % (scratch.in1_ptr[1],
                                                                  scratch.in2_ptr[1],
                                                                  scratch.carry_ptr[1],
                                                                  scratch.out_ptr[1]))
                # Update Environment if MOVE or WRITE
                if prog_id == MOVE_PID or prog_id == UNSET_PID:
                    d_pad.execute(prog_id, arg)
                
                
                break

            else:
                prog_id = np.argmax(n_p)
                prog_name = PROGRAM_SET[prog_id][0]
                if prog_id == MOVE_PID or prog_id == UNSET_PID:
                    arg = [np.argmax(n_args[0]), np.argmax(n_args[1])]
                else:
                    arg = []
                term = False

            cont = 'c'
            # cont = input('Continue? ')