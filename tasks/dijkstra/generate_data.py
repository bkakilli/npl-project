"""
generate_data.py

Core script for generating training/test addition data. First, generates random pairs of numbers,
then steps through an execution trace, computing the exact order of subroutines that need to be
called.
"""
import pickle

import numpy as np

from tasks.dijkstra.trace import DijkstraTrace
from tasks.dijkstra.d import generate_graph


def generate_dijkstra(prefix, num_examples):
    """
    Generates dijkstra data with the given string prefix (i.e. 'train', 'test') and the specified
    number of examples.

    :param prefix: String prefix for saving the file ('train', 'test')
    :param num_examples: Number of examples to generate.
    """
    data = []
    for i in range(num_examples):
        graph = generate_graph(nodes_count=5)
        trace = DijkstraTrace(graph).data
        data.append(( graph, trace ))

    with open('tasks/dijkstra/data/{}.pik'.format(prefix), 'wb') as f:
        pickle.dump(data, f)