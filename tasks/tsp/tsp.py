import random # for fair randomness
import pandas as pd # for visualization
from math import inf as oo # for greedy
import time # for graphs
import matplotlib.pyplot as plt # B for graphs
from itertools import permutations # for greedy
import numpy as np

def print_cities_and_distances(cities, distances):
    """
    This is for printing our distances matrix
    """
    df = pd.DataFrame(data=distances, columns=cities, index=cities)
    print(df)
    
def get_cities(cities_count):
    """
    This is for generating cities in 1D array
    """
    return [c for c in range(cities_count)]

def create_random_cities_and_distances(n):
    """
    This is for generating our distances matrix as 2D array
    """
    cities_count = n
    cities = [c for c in range(cities_count)]
    distances = [[] for _ in range(cities_count)]

    for i in range(cities_count):
        for j in range(cities_count):
            if i == j:
                distance = 0
            elif i > j:
                distance = distances[j][i]
            else:
                distance = random.randint(1, 100)
            distances[i].append(distance)
    print_cities_and_distances(cities, distances)
    return distances

#create_random_cities_and_distances(3)

def do_greedy_search(starting_point, distances_matrix):
    cities = get_cities(len(distances_matrix))
    start_point = starting_point  # 0 means A, 1 means B, 2 means C
    if start_point >= len(cities) or start_point < 0:
        start_point = 0
    current_point = start_point
    shortest_distance = oo
    visited_route = list()
    visited_route.append(current_point)
    available_path = cities  # [0,1,2,3] for 4 cities
    sum = 0
    ss = distances_matrix
    dist_trans = []
    

    for city in range(len(cities)):
        for i in range(len(cities)):
            if available_path[i] == current_point:
                dist_trans.append(0)
                continue
            elif shortest_distance > distances_matrix[current_point][available_path[i]]:
                picked_point = available_path[i]
                #dist_trans.append(picked_point)
            shortest_distance = min(shortest_distance, distances_matrix[current_point][available_path[i]])
            dist_trans.append(shortest_distance)
        #ss[i] = dist_trans
        if len(available_path) <= 1:
            sum += distances_matrix[current_point][start_point]
            print("Route: " + str(visited_route))
            print("Distance: " + str(sum))
        else:
            sum += distances_matrix[current_point][picked_point]
            visited_route.append(picked_point)
            available_path.remove(current_point)
            current_point = picked_point
            shortest_distance = oo
        print(visited_route)
            
    return sum, visited_route

graph = np.array([
    [ 0,  6, 1,  1, 1],
    [ 6,  0,  5,  2,  2],
    [1,  5,  0, 1,  5],
    [ 1,  2, 1,  0,  1],
    [1,  2,  5,  1,  0]
])
#do_greedy_search(0,create_random_cities_and_distances(5))
do_greedy_search(0,graph)
