"""
trace.py

Core class definition for a trace object => given a pair of integers to add, builds the execution
trace, calling the specified subprograms.
TSP():
    GET_NEXT_UNVISITED:
        FOR i=[0:N):
            WRITE(CURR_NODE, CURR_DIST)      
            FIND_SHORTEST_NEIGHBOR
            UPDATE CURR_NODE
        UPDATE CURR_DIST   
    RETURN CURR_DIST
"""
import numpy as np

func_dict = {
    'TSP': 0,
    'SET_CURRENT': 1,
    'SET_VISITED': 2,
    'BYPASS': 3,
    'UNSET': 4,
    'MOVE': 5
}
class TSPEnv:

    def __init__(self, graph):
        self.INF = 999

        self.graph = graph
        self.adjacency = (graph > 0)

        self.nodes_count = len(graph)
        self.unvisited = np.ones((self.nodes_count,), dtype=np.bool)
        self.state = np.ones((self.nodes_count, 2), dtype=int) * self.INF
        self.state[:,1] = -1
        self.state[0,0] = 0
        self.v = -1

        # Pointers
        self.UNVISITED_POINTER_ADDR = 0
        self.UNVISITED_POINTER = np.zeros((len(graph,)))
        self.UNVISITED_POINTER[0] = 1

        self.CURRENT_NODE_POINTER_ADDR = 1
        self.CURRENT_NODE_POINTER = np.zeros(len(graph,))
        self.CURRENT_NODE_POINTER[0] = 1
        

    def execute(self, f, args):
        if f == 'UNSET':
            pointer_addr = args[0]
            if pointer_addr == self.UNVISITED_POINTER_ADDR:
                index = self.UNVISITED_POINTER.argmax()
                self.unvisited[index] = False
        elif f == 'MOVE':
            pointer_addr = args[0]
            if pointer_addr == self.UNVISITED_POINTER_ADDR:
                self.UNVISITED_POINTER = np.roll(self.UNVISITED_POINTER, 1)
            elif pointer_addr == self.CURRENT_NODE_POINTER_ADDR:
                self.CURRENT_NODE_POINTER = np.roll(self.CURRENT_NODE_POINTER, 1)
        elif f == 'BYPASS':
            v = self.CURRENT_NODE_POINTER.argmax()
            # OP-2
            neighbors = get_unvisited_neighbors(self.graph, self.v, self.unvisited)
            # print (state)

            # OP-3
            shortest = self.state[v,0]
            edges = self.graph[self.v]
            for n in neighbors:
                if shortest + edges[n] < self.state[n,0]:
                    # Update the state
                    self.state[n,0] = shortest + edges[n]
                    self.state[n,1] = self.v
    
    def make_state_vector(self):
        state_vec = np.concatenate((
            # self.graph.reshape(-1).astype(np.int32),
            # self.adjacency.reshape(-1).astype(np.int32),
            self.state.copy().T.reshape(-1).astype(np.int32),
            self.unvisited.copy().reshape(-1).astype(np.int32),
            self.UNVISITED_POINTER.copy().reshape(-1).astype(np.int32),
            self.CURRENT_NODE_POINTER.copy().reshape(-1).astype(np.int32)
        ), axis=0)
        return state_vec

class Trace():
    def __init__(self, graph):
        self.trace = []
        self.data = []
        self.create_trace(graph)
        self.make_data()

    def append(self, s):
        s['id'] = len(self.trace)
        self.trace.append(s)

        def make_data(self):
        for s in self.trace:
            self.data.append(( (s['func'], func_dict[s['func']]), s['return'], s['terminate'] ))


    def __str__(self):

        t_str = ""
        for i in range(len(self.trace)):
            s = self.trace[i]
            t_str += 'Computing function %s: \n' % s['func']
            t_str += 'STATE: %s\n' % (str(s['state_vec'][50:]))
            t_str += 'State In:\n' + str(s['state']) + '\n'
            t_str +=  '\n\n'
            t_str += "ACTION: \n"
            t_str += 'Next program: %s' % ('TERMINATE' if s['terminate'] else self.trace[i+1]['func']) + '\n'
            t_str += '------------------------\n'
        
        t_str += '\n'
        return t_str

        def __len__(self):
        return len(self.trace)

     def create_trace(self, graph):

        self.graph = graph
        d_pad = tsp.TSPEnv(graph)

        def new_trace_elem(func, args, terminate=False):
            trace_elem = {
                'func': func,
                'state': d_pad.state.copy(),
                'unvisited': d_pad.unvisited.copy(),
                'unvisited_pointer': d_pad.UNVISITED_POINTER.copy(),
                'current_node_pointer': d_pad.CURRENT_NODE_POINTER.copy(),
                'state_vec': d_pad.make_state_vector(),
                'return': args,
                'terminate': terminate
            }

            return trace_elem

        trace_elem = new_trace_elem('TSP', [])
        self.append(trace_elem)

        