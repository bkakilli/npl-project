"""
train.py

Core training script for the TSP task-specific NPI. Instantiates a model, then trains using
the precomputed data.
"""
from model.npi import NPI
from tasks.tsp.config import CONFIG, get_args
import pickle
import tensorflow as tf

MOVE_PID, UNSET_PID = func_dict['MOVE'], func_dict['UNSET']
DATA_PATH = "tasks/tsp/data/train.pik"
LOG_PATH = "tasks/tsp/log/"


def train_dijkstra(epochs, verbose=0):

    # Load Data
    with open(DATA_PATH, 'rb') as f:
        data = pickle.load(f)

	// Initialize TSP core here

    # Initialize NPI Model
    print ('Initializing NPI Model!')
    npi = NPI(core, CONFIG, LOG_PATH, verbose=verbose)

    # Initialize TF Saver
    saver = tf.train.Saver()

    # Initialize TF Session
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        # Start Training
        for ep in range(1, epochs + 1):
            for i in range(len(data)):
                # Reset NPI States
                npi.reset_state()

                # Setup Environment
                graph, steps = data[i]
                d_pad = DijkstraEnv(graph)
                x, y = steps[:-1], steps[1:]

                # Run through steps, and fit!
                step_def_loss, step_arg_loss, term_acc, prog_acc, = 0.0, 0.0, 0.0, 0.0
                arg0_acc, arg1_acc, arg2_acc, num_args = 0.0, 0.0, 0.0, 0
                for j in range(len(x)):
                    (prog_name_in, prog_in_id), arg, term = x[j]
                    (prog_name_out, prog_out_id), arg_out, term_out = y[j]

                    # print ("%s(%s) calls %s(%s)" % (prog_name_in, str(arg), prog_name_out, str(arg_out)))

                    # Update Environment if MOVE or WRITE
                    if prog_in_id == MOVE_PID or prog_in_id == UNSET_PID:
                        d_pad.execute(prog_in_id, arg)

                    # Get Environment, Argument Vectors
                    env_in = [d_pad.make_state_vector()]
                    arg_in, arg_out = [get_args(arg, arg_in=True)], get_args(arg_out, arg_in=False)
                    prog_in, prog_out = [[prog_in_id]], [prog_out_id]
                    term_out = [1] if term_out else [0]

                    # Fit!
                    if prog_out_id == MOVE_PID or prog_out_id == UNSET_PID:
                        loss, t_acc, p_acc, a_acc, _ = sess.run(
                            [npi.arg_loss, npi.t_metric, npi.p_metric, npi.a_metrics, npi.arg_train_op],
                            feed_dict={npi.env_in: env_in, npi.arg_in: arg_in, npi.prg_in: prog_in,
                                    npi.y_prog: prog_out, npi.y_term: term_out,
                                    npi.y_args[0]: [arg_out[0]],})
                        step_arg_loss += loss
                        term_acc += t_acc
                        prog_acc += p_acc
                        arg0_acc += a_acc[0]
                        num_args += 1
                    else:
                        loss, t_acc, p_acc, _ = sess.run(
                            [npi.default_loss, npi.t_metric, npi.p_metric, npi.default_train_op],
                            feed_dict={npi.env_in: env_in, npi.arg_in: arg_in, npi.prg_in: prog_in,
                                    npi.y_prog: prog_out, npi.y_term: term_out})
                        step_def_loss += loss
                        term_acc += t_acc
                        prog_acc += p_acc

                print ("Epoch {0:02d} Step {1:03d} Default Step Loss {2:05f}, " \
                    "Argument Step Loss {3:05f}, Term: {4:03f}, Prog: {5:03f}, A0: {6:03f}" \
                        .format(ep, i, step_def_loss / len(x), step_arg_loss / len(x), term_acc / len(x),
                            prog_acc / len(x), arg0_acc / num_args))

            # Save Model
            saver.save(sess, 'tasks/tsp/log/model.ckpt')