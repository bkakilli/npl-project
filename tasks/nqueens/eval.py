"""
eval.py

Loads in an Nqueens NPI, and starts a REPL for interactive nqueens.
"""
from model.npi import NPI
from tasks.nqueens.nqueens import NqueensCore
from tasks.nqueens.config import CONFIG
import numpy as np
import pickle
import tensorflow as tf

LOG_PATH = "tasks/nqueens/log/"
CKPT_PATH = "tasks/nqueens/log/model.ckpt"
TEST_PATH = "tasks/nqueens/data/test.pik"
MOVE_PID, WRITE_PID = 0, 1
W_PTRS = {0: "OUT", 1: "CARRY"}
PTRS = {0: "IN1_PTR", 1: "IN2_PTR", 2: "CARRY_PTR", 3: "OUT_PTR"}
R_L = {0: "LEFT", 1: "RIGHT"}


def evaluate_nqueens():
    """
    Load NPI Model from Checkpoint.
    """
    # Load Data
    with open(TEST_PATH, 'rb') as f:
        data = pickle.load(f)

    # Initialize Nqueens Core
    core = NqueensCore()

    # Initialize NPI Model
    npi = NPI(core, CONFIG, LOG_PATH)

    with tf.Session() as sess:
        # Restore from Checkpoint
        saver = tf.train.Saver()
        saver.restore(sess, CKPT_PATH)

        # Run REPL
        repl(sess, npi, data)


def repl(session, npi, data):
    while True:
        inpt = input('Enter One Number between 1 and 10, or Hit Enter for Random: ')

        if inpt == "":
            x, _ = data[np.random.randint(len(data))]

        else:
            x = map(int, inpt.split())

        # Reset NPI States
        print ()
        npi.reset_state()

        # Setup Environment
        prog_name, prog_id, arg, term = 'ADD', 2, [], False

        cont = 'c'
        while cont == 'c' or cont == 'C':
            # Print Step Output
            if prog_id == MOVE_PID:
                a0, a1 = PTRS.get(arg[0], "OOPS!"), R_L[arg[1]]
                a_str = "[%s, %s]" % (str(a0), str(a1))
            elif prog_id == WRITE_PID:
                a0, a1 = W_PTRS[arg[0]], arg[1]
                a_str = "[%s, %s]" % (str(a0), str(a1))
            else:
                a_str = "[]"

            print ('Step: %s, Arguments: %s, Terminate: %s' % (prog_name, a_str, str(term)))
            cont = 'c'
            # cont = input('Continue? ')