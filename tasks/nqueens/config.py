"""
config.py

Configuration Variables for the Addition NPI Task => Stores Scratch-Pad Dimensions, Vector/Program
Embedding Information, etc.
"""
import numpy as np


CONFIG = {
    "ENVIRONMENT_VECTOR_LEN": 25,

    "ARGUMENT_NUM": 1,            # Maximum Number of Program Arguments
    "ARGUMENT_DEPTH": 2,          # Size of Argument Vector => One-Hot, Options 0-1, Default (2)
    "DEFAULT_ARG_VALUE": 0,       # Default Argument Value

    "PROGRAM_NUM": 6,             # Maximum Number of Subroutines
    "PROGRAM_KEY_SIZE": 5,        # Size of the Program Keys
    "PROGRAM_EMBEDDING_SIZE": 10  # Size of the Program Embeddings
}
PROGRAM_SET= [
    ("SHIFT", 3, 2),      # Moves Pointer (3 options) either up or down or right
    ("CHECK", 3),         # Check availablity (3 options)
    ("SET_QUEEN",),               # Set queen on current position
    ("RM_QUEEN",)                # Remove queen from current position
 ]
PROGRAM_ID = {x[0]: i for i, x in enumerate(PROGRAM_SET)}

class Arguments():             # Program Arguments
    def __init__(self, args, num_args=CONFIG["ARGUMENT_NUM"], arg_depth=CONFIG["ARGUMENT_DEPTH"]):
        self.args = args
        self.arg_vec = np.zeros((num_args, arg_depth), dtype=np.float32)


def get_args(args, arg_in=True):
    if arg_in:
        arg_vec = np.zeros((CONFIG["ARGUMENT_NUM"], CONFIG["ARGUMENT_DEPTH"]), dtype=np.int32)
    else:
        arg_vec = [np.zeros((CONFIG["ARGUMENT_DEPTH"]), dtype=np.int32) for _ in
                   range(CONFIG["ARGUMENT_NUM"])]
    if len(args) > 0:
        for i in range(CONFIG["ARGUMENT_NUM"]):
            if i >= len(args):
                arg_vec[i][CONFIG["DEFAULT_ARG_VALUE"]] = 1
            else:
                arg_vec[i][args[i]] = 1
    else:
        for i in range(CONFIG["ARGUMENT_NUM"]):
            arg_vec[i][CONFIG["DEFAULT_ARG_VALUE"]] = 1
    return arg_vec.flatten() if arg_in else arg_vec

