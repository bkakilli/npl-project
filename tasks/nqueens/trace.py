import numpy as np

import tasks.nqueens.nq as nq
file = open("4.txt","w")

Empty = -999



class NqueensTrace:
    def __init__(self, size):
        self.trace = []
        self.data = []
        self.create_trace(size)
        self.make_data()

        self.N = size
        self.queens = [Empty] * self.N # list of columns, where the index represents the row
        self.board = np.zeros([self.N, self.N])
        self.row = 0
        self.col = 0

    def append(self, s):
        s['id'] = len(self.trace)
        self.trace.append(s)
    def make_data(self):
        for s in self.trace:
            self.data.append(( (s['func'], nq.func_dict[s['func']]), s['return'], s['terminate'] ))


    def is_queen_safe(self, row, col):
        for r, c in enumerate(self.queens):
            if (r == row and c != Empty) or (c == col) or abs(row - r) == abs(col - c):
                return False
        return True

    def set_queen(self, row, col):
        if row >= self.N or col >= self.N:
            return
        self.queens[row] = col
        self.board[row][col] = 1

    def rm_queen(self, row, col):

        if row >= self.N or col >= self.N:
            return
        self.queens[row] = Empty
        self.board[row][col] = 0

    def shift_right(self, row, col):
        return row, col+1

    def shift_right_end(self, row, col):
        return row, self.N

    def shift_down(self, row, col):
        return row+1, 0

    def shift_up(self, row):
        col = self.queens[row-1]
        self.board[row - 1][col] = 0
        self.queens[row] = Empty
        return row-1, col+1

    def print_board(self):
        print(self.row, self.col)
        print(self.queens)
        file.write(str(self.queens)+"\n")
        for row in range(self.N):
            print(self.board[row])
            file.write(str(self.board[row])+"\n")
        print("\n")
        file.write("\n")

    def found_sol(self, count):
        print("Success: Solution %s\n\n" % count)
        file.write("Success: Solution %s\n\n" % count)

    def solution(self):
        self.queens = [Empty] * self.N
        self.col = 0
        self.row = 0
        count = 0
        while True:
            while self.col < self.N and not self.is_queen_safe(self.row, self.col):
                self.set_queen(self.row, self.col)
                self.print_board()
                self.rm_queen(self.row, self.col)
                self.print_board()
                self.row, self.col = self.shift_right(self.row, self.col)
            if self.col < self.N:

                # self.queens.append(col)
                self.set_queen(self.row, self.col)
                # print(self.queens)
                self.print_board()

                if self.row + 1 >= self.N:
                    count += 1
                    self.found_sol(count)
                    self.print_board()
                    self.rm_queen(self.row, self.col)
                    self.print_board()
                    self.row, self.col = self.shift_right_end(self.row, self.col)
                else:
                    self.row, self.col = self.shift_down(self.row, self.col)
            if self.col >= self.N:
                # not possible to place a queen in this row anymore
                if self.row == 0:
                    return # all combinations were tried
                self.row, self.col = self.shift_up(self.row)
                self.print_board()

    def generate_board(self):
        return 4

    def create_trace(self, size):

        self.size = size
        d_pad = nq.NqueensEnv(size)

        def new_trace_elem(func, args, terminate=False):
            trace_elem = {
                'func': func,
                'state': d_pad.board.copy(),
                'unvisited': d_pad.queens.copy(),
                'row': d_pad.row.copy(),
                'col': d_pad.col.copy(),
                'state_vec': d_pad.make_state_vector(),
                'return': args,
                'terminate': terminate
            }

            return trace_elem

        trace_elem = new_trace_elem('DIJSKTRA', [])
        self.append(trace_elem)

        while d_pad.queens[self.size-1] < 0:

            # OP-1
            trace_elem = new_trace_elem('NQUEENS', [])
            self.append(trace_elem)

            while d_pad.col < d_pad.N and not d_pad.is_queen_safe(d_pad.row, d_pad.col):
                trace_elem = new_trace_elem('SHIFT_RIGHT', [d_pad.row, d_pad.col])
                self.append(trace_elem)
                d_pad.solution()
                if d_pad.col < d_pad.N:
                    d_pad.set_queen(d_pad.row, d_pad.col)
                    if d_pad.row + 1 >= d_pad.N:
                        self.trace[-1]['terminate'] = True
                        trace_elem = new_trace_elem('RM_QUEEN', [d_pad.row, d_pad.col])
                        self.append(trace_elem)
                        trace_elem = new_trace_elem('SHIFT_RIGHT', [d_pad.row, d_pad.col])
                        self.append(trace_elem)
                    else:
                        trace_elem = new_trace_elem('SHIFT_DOWN', [d_pad.row, d_pad.col])
                        self.append(trace_elem)
                if d_pad.col >= d_pad.N:
                    # not possible to place a queen in this row anymore
                    if d_pad.row == 0:
                        self.trace[-1]['terminate'] = True
                    trace_elem = new_trace_elem('SHIFT_UP', [d_pad.row, d_pad.col])
                    self.append(trace_elem)

# q = Board(4)
# q.solution()
# file.close()