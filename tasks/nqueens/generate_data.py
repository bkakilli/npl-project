"""
generate_data.py

Core script for generating training/test nqueens data. First, generates random pairs of numbers,
then steps through an execution trace, computing the exact order of subroutines that need to be
called.
"""
import pickle

import numpy as np

from tasks.nqueens.trace import NqueensTrace
from tasks.nqueens.nq import NqueensEnv


def generate_nqueens(prefix, size_example):
    """
    Generates nqueens data with the given string prefix (i.e. 'train', 'test') and the specified
    number of examples.

    :param prefix: String prefix for saving the file ('train', 'test')
    :param num_examples: Number of examples to generate.
    """
    data = []
    for i in range(size_example):
        board = NqueensEnv.generate_board(size_example)
        trace = NqueensTrace(board).data
        data.append(( board, trace ))

    with open('tasks/nqueens/data/{}.pik'.format(prefix), 'wb') as f:
        pickle.dump(data, f)