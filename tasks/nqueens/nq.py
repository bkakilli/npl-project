import numpy as np
# file = open("nq.txt","w")

func_dict = {
    'NQUEENS' : 0,
    'SET_QUEEN' : 1,
    'RM_QUEEN': 2,
    'SHIFT_RIGHT': 3,
    'SHIFT_DOWN': 4,
    'SHIFT_UP': 5,
    'CHECK': 6
}

class NqueensEnv:
    def __init__(self, size):
        self.Empty = -999
        self.N = size
        self.queens = [] # list of columns, where the index represents the row
        self.board = np.zeros([self.N, self.N])
        self.row = 0
        self.col = 0

    def is_queen_safe(self, row, col):
        for r, c in enumerate(self.queens):
            if (r == row and c != self.Empty) or c == col or abs(row - r) == abs(col - c):
                return False
        return True

    def set_queen(self, row, col):
        if row >= self.N or col >= self.N:
            return
        self.queens[row] = col
        self.board[row][col] = 1

    def rm_queen(self, row, col):

        if row >= self.N or col >= self.N:
            return
        self.queens[row] = self.Empty
        self.board[row][col] = 0

    def shift_right(self, row, col):
        return row, col+1

    def shift_right_end(self, row, col):
        return row, self.N

    def shift_down(self, row, col):
        return row+1, 0

    def shift_up(self, row):

        col = self.queens[row-1]
        self.queens[row-1] = self.Empty
        self.board[row-1][col] = 0
        return row-1, col+1

    def print_board(self):
        print(self.row, self.col)
        # file.write("Current Pointer: ["+str(self.row)+", "+str(self.col)+"]\n")
        print(self.queens)
        # file.write("Queens List: "+str(self.queens)+"\nBoard: \n")
        for row in range(self.N):
            print(self.board[row])
            # file.write(str(self.board[row])+"\n")
        print("\n")
        # file.write("\n")

    def found_sol(self, count):
        print("Success: Solution %s\n\n" % count)
        # file.write("Success: Solution %s\n\n" % count)

    def solution(self):
        self.queens = [self.Empty] * self.N
        self.col = 0
        self.row = 0
        count = 0
        while True:
            while self.col < self.N and not self.is_queen_safe(self.row, self.col):
                self.set_queen(self.row, self.col)
                self.print_board()
                self.rm_queen(self.row, self.col)
                self.print_board()
                self.row, self.col = self.shift_right(self.row, self.col)
            if self.col < self.N:

                # self.queens.append(col)
                self.set_queen(self.row, self.col)
                # print(self.queens)
                self.print_board()

                if self.row + 1 >= self.N:
                    count += 1
                    self.found_sol(count)
                    self.print_board()
                    self.rm_queen(self.row, self.col)
                    self.print_board()
                    self.row, self.col = self.shift_right_end(self.row, self.col)
                else:
                    self.row, self.col = self.shift_down(self.row, self.col)
            if self.col >= self.N:
                # not possible to place a queen in this row anymore
                if self.row == 0:
                    return # all combinations were tried
                self.row, self.col = self.shift_up(self.row)
                self.print_board()

    def generate_board(self):
        return 4

    def make_state_vector(self):
        state_vec = np.concatenate((
            # self.graph.reshape(-1).astype(np.int32),
            # self.adjacency.reshape(-1).astype(np.int32),
            self.board.copy().T.reshape(-1).astype(np.int32),
            self.queens.copy(),
            self.row,
            self.col
        ), axis=0)
        return state_vec

# Manual operations
q = NqueensEnv(4)
q.solution()
# file.close()