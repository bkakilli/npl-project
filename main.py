"""
main.py
"""
import tensorflow as tf

from tasks.addition.env.generate_data import generate_addition
from tasks.addition.eval import evaluate_addition
from tasks.addition.train import train_addition

from tasks.dijkstra.generate_data import generate_dijkstra
# from tasks.dijkstra.eval import evaluate_dijkstra
from tasks.dijkstra.train import train_dijkstra

from tasks.nqueens.generate_data import generate_nqueens
from tasks.nqueens.eval import evaluate_nqueens
from tasks.nqueens.train import train_nqueens


FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string("task", "dijkstra", "Which NPI Task to run - [addition].")
tf.app.flags.DEFINE_boolean("generate", False, "Boolean whether to generate training/test data.")
tf.app.flags.DEFINE_integer("num_training", 1000, "Number of training examples to generate.")
tf.app.flags.DEFINE_integer("num_test", 100, "Number of test examples to generate.")

tf.app.flags.DEFINE_boolean("do_train", True, "Boolean whether to continue training model.")
tf.app.flags.DEFINE_boolean("do_eval", False, "Boolean whether to perform model evaluation.")
tf.app.flags.DEFINE_integer("num_epochs", 5, "Number of training epochs to perform.")


def main(_):

    elif FLAGS.task == "nqueens":
        # Generate Data (if necessary)
        if FLAGS.generate:
            generate_nqueens('train', FLAGS.num_training)
            generate_nqueens('test', FLAGS.num_test)

        # Train Model (if necessary)
        if FLAGS.do_train:
            train_nqueens(FLAGS.num_epochs)

        # Evaluate Model
        if FLAGS.do_eval:
            evaluate_nqueens()

    elif FLAGS.task == "dijkstra":
        # Generate Data (if necessary)
        if FLAGS.generate:
            generate_dijkstra('train', FLAGS.num_training)
            generate_dijkstra('test', FLAGS.num_test)

        # Train Model (if necessary)
        if FLAGS.do_train:
            train_dijkstra(FLAGS.num_epochs)

        # Evaluate Model
        if FLAGS.do_eval:
            evaluate_dijkstra()


if __name__ == "__main__":
    tf.app.run()